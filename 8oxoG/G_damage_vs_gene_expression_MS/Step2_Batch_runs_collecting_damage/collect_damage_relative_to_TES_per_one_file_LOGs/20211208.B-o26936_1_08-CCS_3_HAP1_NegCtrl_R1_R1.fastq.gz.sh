#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=00:30:00
#SBATCH --job-name=20211208.B-o26936_1_08-CCS_3_HAP1_NegCtrl_R1_R1.fastq.gz
#SBATCH --mem-per-cpu=8G
#SBATCH --output="/cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TES_per_one_file_LOGs/20211208.B-o26936_1_08-CCS_3_HAP1_NegCtrl_R1_R1.fastq.gz.out"
#SBATCH --error="/cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TES_per_one_file_LOGs/20211208.B-o26936_1_08-CCS_3_HAP1_NegCtrl_R1_R1.fastq.gz.err"
#SBATCH --open-mode=truncate

module load python/3.7.4

ls /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/GLOEseq_all_ET743_AF_data_processed

python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TES_per_one_file.py -p /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/ClickCodeSeq3_Dec2021/ClickCodeSeq3_Dec2021_processed_MS/ -s 20211208.B-o26936_1_08-CCS_3_HAP1_NegCtrl_R1_R1.fastq.gz
