#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=01:30:00
#SBATCH --job-name=20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz
#SBATCH --mem-per-cpu=10G
#SBATCH --output="/cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/LOGs/20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz.out"
#SBATCH --error="/cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/LOGs/20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz.err"
#SBATCH --open-mode=truncate

module load python/3.7.4

echo "1"
python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_summed_by_feature__per_one_file.py -p /cluster/scratch/vtakhaveev/ClickCodeSeq3_HMAF_Feb2022_processed_MS -s 20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz -n A

echo "2"
python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_gene_start_and_end_per_one_file.py -p /cluster/scratch/vtakhaveev/ClickCodeSeq3_HMAF_Feb2022_processed_MS -s 20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz -n A

echo "3"
python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file.py -p /cluster/scratch/vtakhaveev/ClickCodeSeq3_HMAF_Feb2022_processed_MS -s 20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz -n A

echo "4"
python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/A_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TES_per_one_file.py -p /cluster/scratch/vtakhaveev/ClickCodeSeq3_HMAF_Feb2022_processed_MS -s 20220222.B-o27508_1_8-CCS_3_U2OS_HMAF_R2_R1.fastq.gz -n A

