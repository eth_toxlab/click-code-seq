'''
Dr. Vakil Takhaveev
21.11.2023
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder with input data", required = True)
parser.add_argument("-s", "--scriptfolder", help = "The path to the folder with scripts", required = True)
parser.add_argument("-n", "--nucleotide", help = "The nucleotide of interest", required = True)

argument = parser.parse_args()

PATH = argument.path
SCRIPTFOLDER = argument.scriptfolder
#"/cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/CCS.v3_HMAF_March2022/G_damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage"
NUCL = argument.nucleotide


bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=01:30:00
#SBATCH --job-name=_SAMPLE_
#SBATCH --mem-per-cpu=10G
#SBATCH --output="_SCRIPTFOLDER_/LOGs/_SAMPLE_.out"
#SBATCH --error="_SCRIPTFOLDER_/LOGs/_SAMPLE_.err"
#SBATCH --open-mode=truncate

module load python/3.7.4

echo "1"
python3.7 _SCRIPTFOLDER_/collect_damage_summed_by_feature__per_one_file.py -p _PATH_ -s _SAMPLE_ -n _NUCL_

echo "2"
python3.7 _SCRIPTFOLDER_/collect_damage_relative_to_gene_start_and_end_per_one_file.py -p _PATH_ -s _SAMPLE_ -n _NUCL_

echo "3"
python3.7 _SCRIPTFOLDER_/collect_damage_relative_to_TSS_per_one_file.py -p _PATH_ -s _SAMPLE_ -n _NUCL_

echo "4"
python3.7 _SCRIPTFOLDER_/collect_damage_relative_to_TES_per_one_file.py -p _PATH_ -s _SAMPLE_ -n _NUCL_

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

for SAMPLE in os.listdir(PATH):
    print(SAMPLE)

    sh_name = os.path.join(SCRIPTFOLDER + "/LOGs/", SAMPLE + ".sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_SAMPLE_" : SAMPLE, "_PATH_" : PATH, "_SCRIPTFOLDER_" : SCRIPTFOLDER, "_NUCL_" : NUCL})
        )

    os.system("sbatch < " + sh_name)
    time.sleep(2)
