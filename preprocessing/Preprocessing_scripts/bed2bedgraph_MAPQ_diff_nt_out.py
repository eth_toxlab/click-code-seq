'''
Dr. Vakil Takhaveev
21.11.2023

This script converts a bed file to one strand-unaware bedgraph file
or two strand-specific bedgraph files. MAPQ values are saved in an additional column.

IMPORTANT: The damage score is always 1. The dmamage scores were not summed per damage position.

The generated bedgraph-files will be specific for each indicated nucleotide!

'''

import numpy as np
import pandas as pd
import Bio
from Bio.Seq import Seq
import argparse
print(np.__version__, pd.__version__, Bio.__version__)

parser = argparse.ArgumentParser(description = "Optional arguments description")
parser.add_argument("-i", "--seqdatapath", help = "The input bed file", required = True)
parser.add_argument("-s", "--strandspecific", help = "Should I make the output strand-specific?", required = True, choices = ['TRUE', 'FALSE'])
parser.add_argument("-n", "--nucleotides", help = "The list of nucleotides of interest: [ALL], [G]", required = True)
parser.add_argument("-c", "--revcompl", help = "Should I reverse-complement the sequence in the bed file?", required = True, choices = ['TRUE', 'FALSE'])
parser.add_argument("-o", "--out", help = "Path incl. name of the output bedgraph", required = True)

argument = parser.parse_args()
SDPATH = argument.seqdatapath
STRAND = argument.strandspecific
NT_LIST = argument.nucleotides
NT_LIST = NT_LIST.replace("[", "").replace("]", "").split(",")
REVCOMPL = argument.revcompl
OUTPATH = argument.out

df = pd.read_csv(SDPATH, sep = "\t", header = None)
print("bed file shape:", df.shape)

df =  df.loc[:, [0, 1, 2, 4, 5, 6]]
df = df.rename(columns={0 : 'Chr', 1 : "Damage_start", 2 : "Damage_end", 
                        4 : 'MAPQ', 5 : 'Strand', 6 : 'Sequence'})

#df["Damage_start"] = df["Damage_start"].astype(int)
#df["Damage_end"] = df["Damage_end"].astype(int)
#df["MAPQ"] = df["MAPQ"].astype(int)
              
#### Reverse complementing
def rev_comp_func(x):
    seq = Seq(x)
    seq2 = seq.reverse_complement()
    return str(seq2)
    
#Nucleotide subselection 
if NT_LIST[0] != "ALL":
    df.loc[:, 'Sequence'] = df["Sequence"].str.upper()
    
    if REVCOMPL == "TRUE":
        df.loc[:, 'Sequence'] = df["Sequence"].apply(rev_comp_func)
    
    df = df[df["Sequence"].isin(NT_LIST)]
    print("bed file1 shape after nucleotide subselection", df.shape)

if REVCOMPL == "TRUE":
    ### Correcting the strand information
    df.loc[:, 'Strand'] = df['Strand'].apply(lambda x: '-' if x == '+' else '+')

### Sorting
df = df.sort_values(by = ['Chr', 'Damage_start'])

### Damage score
df.loc[:, "Damage_score"] = 1


for nt in NT_LIST:
    print("Nucleotide:", nt)
    
    ####
    #Strand information
    if STRAND == "FALSE":
        tmp = None
        if nt != "ALL":
            tmp = df[df['Sequence'] == nt].copy()
        else:
            tmp = df.copy()
        tmp = tmp.loc[:, ['Chr', "Damage_start", "Damage_end", "Damage_score", "MAPQ"]]
        tmp.to_csv(OUTPATH + nt + "_both_strands.bedgraph", header = False, index = False, sep = "\t")
        
    if STRAND == "TRUE":
        tmp = None
        if nt != "ALL":
            tmp = df[df['Sequence'] == nt].copy()
        else:
            tmp = df.copy()
        
        df_plus = tmp[tmp['Strand'] == "+"].copy()
        df_plus = df_plus.loc[:, ['Chr', "Damage_start", "Damage_end", "Damage_score", "MAPQ"]]
        df_plus.to_csv(OUTPATH + nt + "_plus_strand.bedgraph", header = False, index = False, sep = "\t")

        df_minus = tmp[tmp['Strand'] == "-"].copy()
        df_minus = df_minus.loc[:, ['Chr', "Damage_start", "Damage_end", "Damage_score", "MAPQ"]]
        df_minus.to_csv(OUTPATH + nt + "_minus_strand.bedgraph", header = False, index = False, sep = "\t")
    
