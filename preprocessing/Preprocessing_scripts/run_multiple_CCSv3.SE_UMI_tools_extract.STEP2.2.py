'''
Dr. Vakil Takhaveev
02.04.2023

This script is for parallel implementation of the following:
- removing the UMIs from the reads.
.
CCS data analysis.
CCSv3: UMIpattern = NNNNNNNNNNNNNNNNN, addition = barcode_trimmed_
CCSv2.1: UMIpattern = NNNNN, addition = trimmed_
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "Path", required = True)
parser.add_argument("-r", "--pattern", help = "The UMI pattern at the 5-prime end of the read", required = True)
parser.add_argument("-a", "--addition", help = "The prefix of the input file", required = True)
argument = parser.parse_args()

PATH = argument.path
UMIpattern = argument.pattern
prefix = argument.addition

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=umi_extr._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_._PREFIX_.umi_extr.out
#SBATCH --error=_FOLDER_/_NAME_._PREFIX_.umi_extr.err
#SBATCH --open-mode=truncate

module load python/3.7.4

echo '###Removing the UMI from each read sequence and appending it to the name'

$HOME/.local/bin/umi_tools extract --stdin=_FILE1_ --bc-pattern=_UMIpattern_ --log=_FILE2_ --stdout _FILE3_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
    input_file = os.path.join(PATH, SAMPLE, prefix + SAMPLE)
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size
        
for SAMPLE in os.listdir(PATH):
    FOLDER = os.path.join(PATH, SAMPLE)
    FILE1 = os.path.join(FOLDER, prefix + SAMPLE)
    FILE2 = os.path.join(FOLDER, "umi_tools.log")
    FILE3 = os.path.join(FOLDER, "UMIremoved_" + prefix + SAMPLE)
    
    MB_size = os.path.getsize(FILE1)/(10**6)

    ### Setting the computation time
    comp_time = 1.5*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 10:
        d = 10
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
    ### Setting the computation time
    comp_mem = str(int(round(3*MB_size)))

    print(SAMPLE, comp_time, comp_mem)
    
    sh_name = os.path.join(PATH, SAMPLE, "SE_CCS_umi_tools_extr_cp.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, 
                                            "_FILE1_" : FILE1, "_FILE2_" : FILE2, "_FILE3_" : FILE3,
                                           "_TIME_" : comp_time, "_MEMORY_" : comp_mem,
                                           "_UMIpattern_" : UMIpattern, "_PREFIX_" : prefix})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)
