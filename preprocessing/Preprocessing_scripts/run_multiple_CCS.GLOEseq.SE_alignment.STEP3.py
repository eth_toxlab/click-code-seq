'''
Dr. Vakil Takhaveev
17.02.2022

This script is for parallel implementation of the following:
1) Aligning the reads to the reference genome,
2) Sorting the bam file
.
CCS data analysis.

CCSv2.1: trim5 = "6", addition = "trimmed_" or (for 5-nt UMI case) trim5 = "1", addition = "UMIremoved_trimmed_"
CCSv3: trim5 = "1" or "0", addition = "UMIremoved_barcode_trimmed_" or "unspecific_trimmed_"
GLOE-seq: trim5 = "0", addition = "UMIremoved_trimmed_"
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)
parser.add_argument("-a", "--addition", help = "The prefix of the input file", required = True)
parser.add_argument("-t", "--trim", help = "The number of nucleotides to trim at 5prime end", required = True)

argument = parser.parse_args()
PATH = argument.path
prefix = argument.addition
t5 = argument.trim

bash_script_templ = '''#!/bin/bash

#SBATCH -n 6
#SBATCH --time=_TIME_
#SBATCH --job-name=align._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_._PREFIX_.ALIGN.out
#SBATCH --error=_FOLDER_/_NAME_._PREFIX_.ALIGN.err
#SBATCH --open-mode=truncate

module load bowtie2/2.3.5.1 samtools/1.12

ls -l /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as
# ls -l /cluster/home/vtakhaveev/genome/GRCh38_noalt_as

echo '### Aligning the reads to the references'
bowtie2 -x /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as/GRCh38_noalt_as -U _FILE1_ --trim5 _TRIM5_ -S _FILE2_ -p 6
# bowtie2 -x /cluster/home/vtakhaveev/genome/GRCh38_noalt_as/GRCh38_noalt_as -U _FILE1_ --trim5 _TRIM5_ -S _FILE2_ -p 6

echo '### Clean up read pairing information and flags'
samtools fixmate -O bam -@ 6 _FILE2_ _FILE3_

echo '### Sorting the bam files'
samtools sort -O bam -@ 6 -o _FILE4_ -T _FOLDER_/alignment_GRCh38.p13/tmp__PREFIX_/TEMP _FILE3_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
#  if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
    input_file = os.path.join(PATH, SAMPLE, prefix + SAMPLE)
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size
            
for SAMPLE in os.listdir(PATH):
#  if SAMPLE in ["o308811_10-Sample_11_S53_R1_001.fastq.gz", "o308811_07-Sample_8_S62_R1_001.fastq.gz",
#         "o308811_01-Sample_1_S56_R1_001.fastq.gz", "o308811_11-Sample_12_S54_R1_001.fastq.gz"]:
#  if ("_R1" in SAMPLE) and (os.path.isfile(os.path.join(PATH, SAMPLE, SAMPLE + ".UMIremoved_trimmed_.ALIGN.out")) == False):#if SAMPLE.endswith("R1_001.fastq.gz"):   
    
    FOLDER = os.path.join(PATH, SAMPLE)
    
    if not os.path.exists(os.path.join(FOLDER, 'alignment_GRCh38.p13')):
        os.mkdir(os.path.join(FOLDER, 'alignment_GRCh38.p13'))
    os.mkdir(os.path.join(FOLDER, 'alignment_GRCh38.p13', 'tmp_' + prefix))
    
    FILE1 = os.path.join(FOLDER, prefix + SAMPLE)
    FILE2 = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sam")
    FILE3 = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.fixmate.bam")
    FILE4 = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sorted.bam")
    
    MB_size = os.path.getsize(FILE1)/(10**6)

    ### Setting the computation time
    comp_time = 4*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 15:
        d = 15
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
    ### Setting the computation time
    if MB_size < 500:
        MB_size = 500
    comp_mem = str(int(round(4*MB_size)))#3*

    print(SAMPLE, comp_time, comp_mem)
    
    sh_name = os.path.join(PATH, SAMPLE, "SE_CCS_alignment_" + prefix + "_cp.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, 
                                            "_FILE1_" : FILE1, "_FILE2_" : FILE2, 
                                            "_FILE3_" : FILE3, "_FILE4_" : FILE4,
                                           "_TIME_" : comp_time, "_MEMORY_" : comp_mem,
                                            "_TRIM5_": t5, '_PREFIX_' : prefix})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)
