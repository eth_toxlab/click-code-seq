'''
Dr. Vakil Takhaveev
21.11.2023

This script implements the following:
1) Filtering out unmapped reads,
2) Generating stats after filtering out unmapped reads,
3) Indexing,
4) Generates a bed file. Important: no omission of signal at the ends of chromosomes!
5) Generates bedgraph-like files with the additional column with MAPQ.

The generated bedgraph-files will be specific for each indicated nucleotide!
.
CCS and GLOE-Seq data analysis.

The argument cheat sheet:
GLOE-Seq: addition = UMIdedup_UMIremoved_trimmed_, offset = 1
CCSv3: addition = UMIdedup_UMIremoved_barcode_trimmed_, offset = 0
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "Path", required = True)
parser.add_argument("-a", "--addition", help = "The prefix of the input file", required = True)
parser.add_argument("-o", "--offset", help = "The difference between the coordinate of the 5prime-end of the region and the coordinate of the damage", required = True)
parser.add_argument("-n", "--nucleotides", help = "Nucleotides at the damage site", required = True)

argument = parser.parse_args()

PATH = argument.path
prefix = argument.addition
offset = argument.offset
nucleotides = argument.nucleotides
nucl_text = nucleotides.replace('[', '').replace(']', '').replace(',', '_')

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=_PREFIX_.bed_graph_MAPQ._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_._PREFIX_._NUCLTEXT_.bed_graph_MAPQ.out
#SBATCH --error=_FOLDER_/_NAME_._PREFIX_._NUCLTEXT_.bed_graph_MAPQ.err
#SBATCH --open-mode=truncate

module load python/3.7.4 samtools/1.12 bedtools2/2.29.2

ls _FOLDER_

mkdir _FOLDER_/bed_and_bedgraph

echo '### Filtering out unmapped reads'
samtools view -b -F 4 _FILE1_ > _FILE2_

echo '### Generating stats after filtering unmapped reads'
samtools stats _FILE2_ > _FILE2STATS_

echo '### Indexing'
samtools index _FILE2_

echo '### Bam2Bed'
bedtools bamtobed -i _FILE2_ > _FILE3_

echo '###Extracting coordinates of the damage position'
awk -v OFS='\t' '{if ($6=="+") {$3=$2+1-_OFFSET_;$2=$2-_OFFSET_} else {$2=$3-1+_OFFSET_;$3=$3+_OFFSET_}} {print}' _FILE3_ > _FILE4_

echo '###Number of cases when the damage position is negative'
awk -v OFS='\t' '$2==-1 {print}' _FILE4_ | wc -l

echo '###Removing the cases when the damage position is negative (only possible in ChrM)'
awk -v OFS='\t' '$2>=0 {print}' _FILE4_ > _FILE5_

echo '###Identifying the nucleotide of the damage position'
bedtools getfasta -fi /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _FILE5_ -bedOut -s > _FILE6_

echo '###Bedgraph'
python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/ClickCodeSeq/ClickCodeSeq_scripts/Metaprofile_scripts/bed2bedgraph_MAPQ_diff_nt_out.py -i _FILE6_ -o _FILE7_ -n _NUCLEOTIDES_ -c TRUE -s TRUE

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
#  if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
    input_file = os.path.join(PATH, SAMPLE, "dedupl_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.dedupl.bam")
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size
        
for SAMPLE in os.listdir(PATH):
#  if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
    FOLDER = os.path.join(PATH, SAMPLE)
    FILE1 = os.path.join(FOLDER, "dedupl_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.dedupl.bam")
    FILE2 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.dedupl.filtered.bam")
    FILE2_STATS = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.dedupl.filtered.STATS.txt")
    FILE3 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.dedupl.filtered.bam.bed")
    FILE4 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.DAMAGE.bed")
    FILE5 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.DAMAGE._1corr.bed")
    FILE6 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13.DAMAGE._1corr.SEQ.bed")
    FILE7 = os.path.join(FOLDER, "bed_and_bedgraph", prefix + SAMPLE + "GRCh38.p13_")

    MB_size = os.path.getsize(FILE1)/(10**6)
    
    ### Setting the computation time
    comp_time = 1.0*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 20:
        d = 20
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
    ### Setting the computation time
    if MB_size < 150:
        MB_size = 150
    comp_mem = str(int(round(5*MB_size)))

    print(SAMPLE, comp_time, comp_mem)

    sh_name = os.path.join(PATH, SAMPLE, "SE_CCS_bed_graph_MAPQ_" + prefix + "_" + nucl_text + "_cp.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, 
                                            "_FILE1_" : FILE1, "_FILE2_" : FILE2, 
                                            "_FILE3_" : FILE3, "_FILE4_" : FILE4,
                                            "_FILE5_" : FILE5, "_FILE6_" : FILE6,
                                            "_FILE7_" : FILE7,
                                            "_FILE2STATS_" : FILE2_STATS,
                                            '_NUCLEOTIDES_' : nucleotides, '_NUCLTEXT_' : nucl_text,
                                            "_TIME_" : comp_time, "_MEMORY_" : comp_mem,
                                            "_PREFIX_" : prefix, "_OFFSET_" : offset})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)
