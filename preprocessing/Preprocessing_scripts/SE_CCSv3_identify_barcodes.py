'''
Vakil Takhaveev, PhD
21.01.2022

This script searches for the expected barcodes at the 5' end of each read.

CCS.v3
'''
import argparse
import gzip
from Bio import SeqIO
import os

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-i", "--input", help = "Input fastq.gz file", required = True)
argument = parser.parse_args()

inputfile = argument.input
handle_in = gzip.open(inputfile, "rt")

path, filename = os.path.split(inputfile)

handle_out1 = gzip.open(os.path.join(path, "barcode_" + filename), "wt")
handle_out2 = gzip.open(os.path.join(path, "unspecific_" + filename), "wt")

Nb = 0
Nu = 0

for record in SeqIO.parse(handle_in, "fastq"):
    ccadapter = record.seq[:18]
    
    if ccadapter[10:17] == "TTGTTGA":
        handle_out1.write(record.format("fastq"))
        Nb += 1
        
    elif (ccadapter[0:6] == "TTGTTG") & (ccadapter[16] == "A"):
        handle_out1.write(record.format("fastq"))
        Nb += 1
        
    else:
        handle_out2.write(record.format("fastq"))
        Nu += 1
        
print("Barcode list:", Nb, Nb/(Nb + Nu))
print("Unspecific list:", Nu, Nu/(Nb + Nu))
