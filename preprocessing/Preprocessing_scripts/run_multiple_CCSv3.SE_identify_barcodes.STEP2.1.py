'''
Dr. Vakil Takhaveev
02.04.2023

This script is for parallel implementation of the following:
- searching for the expected barcodes at the 5' end of each read --> SE_CCSv3_identify_barcodes.py
.
CCSv.3 data analysis.
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)
argument = parser.parse_args()

PATH = argument.path

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=barcodes._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_.barcodes.out
#SBATCH --error=_FOLDER_/_NAME_.barcodes.err
#SBATCH --open-mode=truncate

module load python/3.7.4

echo '###Filtering the reads with respect to the barcodes'

python3.7 $HOME/DamageSeqAnalysis/ClickCodeSeq/ClickCodeSeq_scripts/ESSENTIAL_STEPS/SE_CCSv3_identify_barcodes.py -i _FILE_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
    input_file = os.path.join(PATH, SAMPLE, "trimmed_" + SAMPLE)
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size
        
for SAMPLE in os.listdir(PATH):
    FOLDER = os.path.join(PATH, SAMPLE)
    FILE = os.path.join(PATH, SAMPLE, "trimmed_" + SAMPLE)

    MB_size = os.path.getsize(FILE)/(10**6)

    ### Setting the computation time
    comp_time = 4*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 30:
        d = 30
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
    ### Setting the computation time
    comp_mem = str(int(round(4*MB_size)))

    print(SAMPLE, comp_time, comp_mem)

    
    sh_name = os.path.join(PATH, SAMPLE, "SE_CCSv3_identify_barcodes_cp.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, "_FILE_" : FILE,
                                           "_TIME_" : comp_time, "_MEMORY_" : comp_mem})
        )
    os.system("sbatch < " + sh_name)
    time.sleep(2)
