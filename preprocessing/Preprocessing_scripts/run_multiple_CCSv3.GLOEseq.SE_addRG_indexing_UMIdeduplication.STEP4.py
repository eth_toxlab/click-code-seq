'''
Dr. Vakil Takhaveev
18.02.2023

This script implements the following:
1) Adding the read goup tag necessary for deduplication,
2) Indexing necessary for deduplication,
3) Generating stats of the alignment file,
4) Deduplicating via umi-tools: method "unique",
5) Generating stats of the deduplicated file
.
CCS data analysis.
CCSv3: addition = UMIremoved_barcode_trimmed_
CCSv2.1: addition = UMIremoved_trimmed_
GLOE-seq: addition = UMIremoved_trimmed_
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "Path", required = True)
parser.add_argument("-a", "--addition", help = "The prefix of the input file", required = True)
argument = parser.parse_args()

PATH = argument.path
prefix = argument.addition

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=UMIdedupl._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_._PREFIX_.UMIdedupl.out
#SBATCH --error=_FOLDER_/_NAME_._PREFIX_.UMIdedupl.err
#SBATCH --open-mode=truncate

module load python/3.7.4 samtools/1.12 

echo '### Adding the read goup tag necessary for deduplication'
samtools addreplacerg -r ID:_NAME_ -m overwrite_all -o _FILE2_ -@ 1 _FILE1_

echo '### Indexing before deduplication'
samtools index _FILE2_

echo '### Stats after sorting and adding RG'
samtools stats -@ 1 _FILE2_ > _FILE2STATS_

echo '### Deduplication'
mkdir _FOLDER_/dedupl_GRCh38.p13
# $HOME/.local/bin/umi_tools dedup -I _FILE2_ --output-stats=_FILE3STATS_ --method=unique -S _FILE3_
# To decrease time and memory usage, I removed the output-stats
$HOME/.local/bin/umi_tools dedup -I _FILE2_ --method=unique -S _FILE3_

echo '### Stats after deduplication'
samtools stats -@ 1 _FILE3_ > _FILE33STATS_

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
#  if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
    input_file = os.path.join(PATH, SAMPLE, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sorted.bam")
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size

for SAMPLE in os.listdir(PATH):
#  if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
    FOLDER = os.path.join(PATH, SAMPLE)
    
    FILE1 = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sorted.bam")
    FILE2 = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sorted.addedRG.bam")
    FILE2_STATS = os.path.join(FOLDER, "alignment_GRCh38.p13", prefix + SAMPLE + "GRCh38.p13.sorted.addedRG.STATS.txt")
    FILE3 = os.path.join(FOLDER, "dedupl_GRCh38.p13", "UMIdedup_" + prefix + SAMPLE + "GRCh38.p13.dedupl.bam")
    FILE3_STATS = os.path.join(FOLDER, "dedupl_GRCh38.p13", "UMIdedup_" + prefix + SAMPLE + "GRCh38.p13.dup_metrics")
    FILE33_STATS = os.path.join(FOLDER, "dedupl_GRCh38.p13", "UMIdedup_" + prefix + SAMPLE + "GRCh38.p13.dedupl.STATS.txt")

    MB_size = os.path.getsize(FILE1)/(10**6)

    ### Setting the computation time
    comp_time = 4.0*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 15:
        d = 15
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
    ### Setting the computation time
    if MB_size < 250:
        MB_size = 250
    comp_mem = str(int(round(10*MB_size)))
    
    print(SAMPLE, comp_time, comp_mem)
    
    sh_name = os.path.join(PATH, SAMPLE, "SE_CCS_UMIdeduplication_cp.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, 
                                            "_FILE1_" : FILE1, "_FILE2_" : FILE2, 
                                            "_FILE3_" : FILE3, "_FILE2STATS_" : FILE2_STATS,
                                            "_FILE3STATS_" : FILE3_STATS, "_FILE33STATS_" : FILE33_STATS,
                                            "_TIME_" : comp_time, "_MEMORY_" : comp_mem,
                                            "_PREFIX_" : prefix})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)
