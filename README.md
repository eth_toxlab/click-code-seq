# Click-code-seq
This GitLab project contains the code for the following research manuscript:

**Click-chemistry-aided quantitation and sequencing of oxidized guanines and apurinic sites uncovers their transcription-linked strand bias in human cells**<br>
Vakil Takhaveev, Nikolai J.L. Püllen, Navnit K. Singh, Sabrina M. Huber, Stefan Schauer, Hailey L. Gahlon, Anna R. Poetsch, Shana J. Sturla

## Abstract
DNA alterations are drivers of aging, neurodegeneration, carcinogenesis, and chemotherapy drug action. To understand the functional genomic roles of DNA modifications, it is critical to accurately map their diverse chemical forms with single-nucleotide precision in complex genomes, which however remains challenging. Click-code-seq is the click-chemistry-aided single-nucleotide-resolution method introduced to map oxidation in yeast DNA. Here, we upgraded click-code-seq to enable its first application for sequencing DNA oxidation and depurination in the human genome. For this, we developed a companion fluorescence tagging assay, click-fluoro-quant, to rapidly quantify common modifications. Moreover, we devised novel adapters to minimize false modification detection and assess modification frequency in cell populations. We uncovered that endogenous DNA oxidation in a human cell line has a highly similar pattern to the cancer mutational signatures associated with the effects of reactive oxygen species. We established that the chemotherapy drug irofulven preferentially depurinates ApA dimers and promoter regions. Intriguingly, we revealed that oxidized guanines and apurinic sites, both irofulven-induced and endogenous, are depleted in the transcribed strand of genes, and the strand bias widens with increasing gene expressions. This work advances click-code-seq for deciphering the impacts of key DNA modifications in the human genome on cellular physiology and toxicological responses.

## License
MIT License, Copyright (c) 2024 Dr. Vakil Takhaveev

## Project status
The manuscript is deposited on bioRxiv.
