#importing necessary modules
import os
os.environ['LC_ALL'] = 'en_US.UTF-8'
os.environ['LANG'] = 'en_US.UTF-8'
os.environ["MPLBACKEND"] = "TkAgg"

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import sys

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

matplotlib.rcParams['font.sans-serif'] = "Arial"
matplotlib.rcParams['font.family'] = "sans-serif"
matplotlib.rcParams['mathtext.default'] = "regular"

matplotlib.rcParams['font.size'] = 16


def barplot_table_under(df, cond_list, BARWIDTH, line_coords, fortable, ylims, xlims, barfacecolors, baredgecolors, markeredgecolor, ylabel, FIGURE_OUTPATH, FIGURE_name, x_ylab, y_ylabel, figsize):
    fig = plt.figure(1, figsize=figsize, dpi = 200, facecolor = "white", constrained_layout=True)
    ax = plt.subplot(1, 1, 1)

    cond_means = []
    ### Data points    
    for index, cond in enumerate(cond_list):
        vals = df[cond].values
        plt.plot([index]*len(vals), vals, 'o', markersize = 8, markeredgecolor = markeredgecolor, markerfacecolor = "None", alpha = 0.5, markeredgewidth = 1.5)
        mean = np.mean(vals)
        cond_means.append(mean)

    ### Bar plot
    cond_indices = np.arange(len(cond_list))
    ax.bar(cond_indices, cond_means, color = barfacecolors, edgecolor = baredgecolors, linewidth = 2, width = BARWIDTH)
    for l in line_coords:
        y_coord = cond_means[l[2]]
        plt.plot([l[0], l[1]], [y_coord, y_coord], color = "black", ls = "dashed", lw = 1)

    ### Table   
    column_width = 1.0/len(cond_list)
    table_height = 0.075*fortable.shape[0]
    table = plt.table(cellText=fortable.values, rowLabels=fortable.index, loc='top', cellLoc='center', fontsize='medium',
                      colWidths=[column_width]*len(cond_list), bbox=[0, -1*(table_height+0.025), 1, table_height], )
    # Iterate over the cells and set the background color to gray if the cell contains '+'
    for (row, col), cell in table.get_celld().items():
        if fortable.iloc[row, col] == '+':
            cell.set_facecolor('gray')
            cell.set_text_props(color='white')
        else:
            cell.set_facecolor('white')
            cell.set_text_props(color='black')
            
        # Set row labels background to white and font to black
        if col == -1:  # This checks if the cell is a row label
            cell.set_facecolor('white')
            cell.set_text_props(color='black')
            
    for key, cell in table.get_celld().items():
        cell.set_linewidth(0)

    plt.ylim(ylims[0], ylims[1])
    plt.xlim(xlims[0], xlims[1])
    ax.set_xticks([])

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    fig.supylabel(t = ylabel, ha = "center", va = "top", x = x_ylab, y = y_ylabel, fontsize = "medium")

    plt.savefig(FIGURE_OUTPATH + "PDFs/" + FIGURE_name + ".pdf")
    plt.savefig(FIGURE_OUTPATH + "PNGs/" + FIGURE_name + ".png")
    plt.close(fig)
    
    return cond_means

def scatter_plot(df, x_var, y_var, xlims, ylims, markeredgecolor, xlabel, ylabel, FIGURE_OUTPATH, FIGURE_name, x_ylab, y_ylabel, figsize):

    fig = plt.figure(1, figsize=figsize, dpi = 200, facecolor = "white")
    ax = plt.subplot(1, 1, 1)
    
    print(scipy.stats.pearsonr(df[x_var], df[y_var]))
    plt.plot(df[x_var], df[y_var], 'o', markersize = 8, markeredgecolor = markeredgecolor, markerfacecolor = "None", alpha = 1, markeredgewidth = 1.5)
    
    coef = np.polyfit(df[x_var], df[y_var], 1)
    poly1d_fn = np.poly1d(coef)
    
    xx = np.array(sorted(set(df[x_var].values)))
    plt.plot(xx, poly1d_fn(xx), '-', linewidth = 2, color = markeredgecolor)
    
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel("")
    plt.ylim(ylims[0], ylims[1])
    plt.xlim(xlims[0], xlims[1])
    ax.set_xticks(xx)

    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    fig.supylabel(t = ylabel, ha = "center", va = "top", x = x_ylab, y = y_ylabel, fontsize = "medium")

    plt.tight_layout(pad = 0.25)
    plt.savefig(FIGURE_OUTPATH + "PDFs/" + FIGURE_name + ".pdf")
    plt.savefig(FIGURE_OUTPATH + "PNGs/" + FIGURE_name + ".png")
    plt.close(fig)
    
    
def barplot_NO_table_under(df, cond_list, xticklabels, rotation, BARWIDTH, line_coords, ylims, xlims, barfacecolors, baredgecolors, markeredgecolor, xlabel, ylabel, FIGURE_OUTPATH, FIGURE_name, x_ylab, y_ylabel, figsize):
    fig = plt.figure(1, figsize=figsize, dpi = 200, facecolor = "white")
    ax = plt.subplot(1, 1, 1)

    cond_means = []
    ### Data points    
    for index, cond in enumerate(cond_list):
        vals = df[cond].values
        plt.plot([index]*len(vals), vals, 'o', markersize = 8, markeredgecolor = markeredgecolor, markerfacecolor = "None", alpha = 0.5, markeredgewidth = 1.5)
        mean = np.mean(vals)
        cond_means.append(mean)

    ### Bar plot
    cond_indices = np.arange(len(cond_list))
    ax.bar(cond_indices, cond_means, color = barfacecolors, edgecolor = baredgecolors, linewidth = 2, width = BARWIDTH)
    for l in line_coords:
        y_coord = cond_means[l[2]]
        plt.plot([l[0], l[1]], [y_coord, y_coord], color = "black", ls = "dashed", lw = 1)

    plt.ylim(ylims[0], ylims[1])
    plt.xlim(xlims[0], xlims[1])
    ax.set_xticks(cond_indices)
    ax.set_xticklabels(xticklabels, rotation = rotation)
    ax.set_xlabel(xlabel)
    
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    fig.supylabel(t = ylabel, ha = "center", va = "top", x = x_ylab, y = y_ylabel, fontsize = "medium")
    
    plt.tight_layout(pad = 0.25)
    plt.savefig(FIGURE_OUTPATH + "PDFs/" + FIGURE_name + ".pdf")
    plt.savefig(FIGURE_OUTPATH + "PNGs/" + FIGURE_name + ".png")
    plt.close(fig)
    
    return cond_means
 