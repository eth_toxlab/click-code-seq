#Vakil Takhaveev, PhD
#07.02.2024
#
#This is a collection of figure-plotting functions.

#importing necessary modules
import os
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import sys

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

matplotlib.rcParams['font.sans-serif'] = "Arial"
matplotlib.rcParams['font.family'] = "sans-serif"
matplotlib.rcParams['mathtext.default'] = "regular"

matplotlib.rcParams['font.size'] = 16


def plot_damage_boxplots(DATA1, samples, groups, feature_order, palette_features, SUFFIX, ylims, yticks, ylabel, FIGURE_OUTPATH):
    for index, s in enumerate(samples):
        df = DATA1[DATA1["Sample"] == s].copy()
        
        print(s)
        tmp = df.loc[df["Feature"] == feature_order[0], ["Group"]].groupby(by = "Group").size()
        print(tmp, tmp.sum())
        print("Not shown:", feature_order[0], df[(df["Feature"] == feature_order[0]) & ((df["Damage"] > ylims[1]) | (df["Damage"] < ylims[0]))].shape[0])
        print("Not shown:", feature_order[1], df[(df["Feature"] == feature_order[1]) & ((df["Damage"] > ylims[1]) | (df["Damage"] < ylims[0]))].shape[0])
        
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        
        ax = plt.subplot(1, 1, 1)
        sns.boxplot(x = "Group", y = "Damage", hue = "Feature", data = df, 
                    order = groups,
                    hue_order = feature_order,
                    orient = "v", fliersize = 0.2, linewidth = 1, saturation = 1,
                    palette = palette_features, showmeans=True,
                    meanprops={"marker" : "*", "markerfacecolor" : "None", "markeredgecolor" : "#2F4F4F", "markersize" : 5},
                    flierprops={"markerfacecolor" : "gray", "markeredgecolor" : "gray"})

        
        t = ax.text(1, 0.02, "1 replicate", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.75, edgecolor='None', linewidth=0))

        leg = ax.legend(loc = 2, frameon = True, fontsize = "medium")
        leg.get_frame().set_linewidth(0.0)

        ax.set_ylabel('')
        ax.set_xlabel("Gene expression tiers")
        ax.set_xticklabels(groups, rotation = 45)
        ax.set_ylim(ylims[0], ylims[1])
        ax.set_yticks(yticks)
        
        #Change in version2
        ##this is to isolate the first group of all genes:
        #ax.axvline(0.5, ls = 'dashed', color = sns.color_palette("bright")[7], lw = 1)
        #
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        
        fig.supylabel(t = ylabel, ha = "center", va = "center",
                      x = 0.05, y = 0.65, fontsize = "medium")
        
        plt.tight_layout(pad = 0.25)
        
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + s.replace(" ", "_") + "_boxplots_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + s.replace(" ", "_") + "_boxplots_" + SUFFIX + ".png")
        plt.close(fig)  
    
    
        
def plot_median_damage_values_and_correlation(DATA1, sample_group, groups, feature_order, palette_features, SUFFIX, 
                                            ylims, yticks, ylabel_part, FLAG_correlation, feature_order_abbrs, yticks_corr, FIGURE_OUTPATH):
    feat_list = []
    i_list = []
    gr_list = []
    median_list = []
    mean_list = []

    rho_list = []
    sample_list = []
    feat_for_rho_list = []

    for index, feat in enumerate(feature_order):
        for s in sample_group:
            df = DATA1[DATA1["Sample"] == s].copy()
            df1 = df[df["Feature"] == feat].copy()
                
            rho, p_val = scipy.stats.spearmanr(df1["Expression_level"], df1["Damage"])
            print("Spearman rho:", feat, s, rho, p_val)
            rho_list.append(rho)
            sample_list.append(s)
            feat_for_rho_list.append(feat)
            
            list_of_lists = []
                
            for i, gr in enumerate(groups):
                l1 = df1[df1["Group"] == gr]["Damage"].values
                list_of_lists.append(list(l1))
                
                M = np.median(l1)
                mean = np.mean(l1)
                    
                median_list.append(M)
                mean_list.append(mean)
                i_list.append(i)
                gr_list.append(gr)
                feat_list.append(feat)
                
            print("Kruskal:", feat, s, scipy.stats.kruskal(*list_of_lists))
            
    tmp = pd.DataFrame({"Group" : gr_list, "i" : i_list, "Median" : median_list, "Feature" : feat_list, "Mean" : mean_list})
    tmp = tmp.sort_values(by = ["Group"], ascending = True)
    tmp = tmp.sort_values(by = ["Feature"], ascending = True)
    print(tmp)
    
    tmp_rho = pd.DataFrame({"Sample" : sample_list, "Feature" : feat_for_rho_list, "Rho" : rho_list})
    tmp_rho.loc[:, "Type"] = SUFFIX
    
    metrics_list = ["Median", "Mean"]
    markers_list = ["o", "*"]
    
    for metric_index, metric in enumerate(metrics_list):
    
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        label = None
        prev_lab = None
        for j, row in tmp.iterrows():
            if row["i"] == 0:
                if row["Feature"] != prev_lab:
                    label = row["Feature"]
                else:
                    label = None

                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = label, lw = 0)

                prev_lab = row["Feature"]
            else:
                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = None, lw = 0)

        plt.legend(loc = 2, bbox_to_anchor = (-0.06, 0., 1, 1), frameon = False)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        lll = sorted(list(tmp["i"].unique()))
        print(lll)
        ax.set_xticks(lll)
        ax.set_xticklabels(groups, rotation = 45)
        ax.set_xlabel("Gene expression tiers")
        ax.set_ylabel("")

        ax.set_ylim(ylims[0], ylims[1])
        ax.set_yticks(yticks)
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)

        ###This is the only difference in Version 2:
        fig.supylabel(t = metric + ' ' + ylabel_part, ha = "center", va = "center", x = 0.05, y = 0.6, fontsize = "medium")
        ###
        
        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + metric + "_DNA_damage_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + metric + "_DNA_damage_" + SUFFIX + ".png")
        plt.close(fig)
        
    
    if FLAG_correlation == True:
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        for s in (tmp_rho["Sample"].unique()):
            xx = []
            yy = []
            for i_f, f in enumerate(feature_order):
                y = tmp_rho[(tmp_rho["Sample"] == s) & (tmp_rho["Feature"] == f)]["Rho"].iat[0]
                xx += [i_f]
                yy += [y]

                ax.plot([i_f], [y], 'o', color = palette_features[f], alpha = 1, markersize = 12)

            ax.plot(xx, yy, '-', color = "gray", alpha = 1, lw = 0.5)

        ax.set_xlabel("Strand")
        ax.set_ylabel("")
        ax.set_xticks([0, 1])
        ax.set_xlim(-0.5, 1.5)
        ax.set_xticklabels(feature_order_abbrs)
        ax.set_yticks(yticks_corr)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        fig.supylabel(t = 'Correlation between ' + ylabel_part + ' level\nand gene expression', ha = "center", va = "top",
                          x = 0.05, y = 0.99, fontsize = "medium")

        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".png")
        plt.close(fig)
