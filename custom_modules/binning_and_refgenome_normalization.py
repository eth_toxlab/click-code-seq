#Vakil Takhaveev, PhD
#06.02.2024
#
#This is a collection of functions that assist gene metaprofile plotting, specifically,
#counting certain nucleotides within a region of interest in the reference genome, and
#binning the damage-seq data and normalizing it by the reference-genome counts.

import os
import numpy as np
import pandas as pd
import scipy
import sys
import math
import pyfaidx
from pyfaidx import Fasta
from itertools import product

###
#Part 1. Binning with relative coordinates.
###
def count_Nucl_in_a_fragment_relative_coords(Nucl_of_interest, gen_ref, chrom, TSS, TES, gene_strand, ref_strand, bin_start, binsize):
    compl_hash = {"A" : "T", "T" : "A", "C" : "G", "G" : "C"}
    #nucleotide to count in the sequence to be extracted 
    nucl = None
    if ref_strand == "+":
        if gene_strand == "sense":
            nucl = Nucl_of_interest#"G"
        if gene_strand == "antisense":
            nucl = compl_hash[Nucl_of_interest]#"C"
    if ref_strand == "-":
        if gene_strand == "sense":
            nucl = compl_hash[Nucl_of_interest]#"C"
        if gene_strand == "antisense":
            nucl = Nucl_of_interest#"G"
    
    START = (TES - TSS)*bin_start + TSS
    END = (TES - TSS)*(bin_start+binsize) + TSS
    #print(START, END)
    
    if ref_strand == "+":
        #to make this comparable to pd.cut, where the lower boundary of the bin is not inclusive
        if float(START).is_integer(): 
            START = int(START) + 1
        else:  
            START = math.ceil(START)

        END = math.floor(END)
        #print(START, END)
        
    if ref_strand == "-":
        #to make this comparable to pd.cut, where the lower boundary of the bin is not inclusive
        if float(START).is_integer(): 
            START = int(START) - 1
        else:  
            START = math.floor(START)
            
        END = math.ceil(END)
        #print(START, END)
    
    sequence = None
    #+1: due to the property of the module pyfaidx
    if ref_strand == "+":
        sequence = gen_ref[chrom][START:(END+1)].seq
    if ref_strand == "-":
        sequence = gen_ref[chrom][END:(START+1)].seq
        
    sequence = sequence.upper()
    Nnucl = sequence.count(nucl)
    
    return Nnucl
    #print(sequence, Nnucl)
    
    
def binning_gene_body(Nucl_of_interest, DATA1, BINSIZE, VARIABLE, norm_df, SUFFIX, gen_ref, DF_gene_coords, OUTPATH):
    #creating bins
    bin_borders = np.arange(0, 1 + BINSIZE, BINSIZE)
    print(bin_borders)
    
    #building a template for the final data frame 
    strands = ["sense", "antisense"]
    bin_list = list(bin_borders[:-1])
    sample_list = DATA1["Sample"].unique().tolist()
    genes = DF_gene_coords["Gene"].unique().tolist()
    
    template = pd.DataFrame(
                        list(product(sample_list, strands, genes, bin_list)),
                        columns=['Sample', 'Strand', 'Gene', 'Bin'])
    print("Template for the final data frame:", template.shape[0])
    
    #building a template data frame for Nucl counting
    template_for_Nucl = pd.DataFrame(
                        list(product(strands, genes, bin_list)),
                        columns=['Strand', 'Gene', 'Bin'])
    print("Template data frame for Nucl counting; Genes x strands x used bins:", template_for_Nucl.shape[0])
    
    #binning
    DATA1.loc[:, 'Bin'] = pd.cut(DATA1[VARIABLE], bins = bin_borders, labels = bin_borders[:-1], 
                                 include_lowest = False, right = True)#include_lowest is now False!!!
    DATA1.loc[:, 'Bin'] = DATA1['Bin'].astype(float)
    
    #correcting the values for the sequencing depth
    DATA1 = pd.merge(DATA1, norm_df, on = "Sample", how = "left")
    DATA1.loc[:, 'Value'] = DATA1['Value']/DATA1['Median']
    
    #counting the number of Nucl per bin
    gene_df = pd.merge(DF_gene_coords, template_for_Nucl, on = ['Gene'], how = "inner")
    print("Template data frame for Nucl counting after merging with gene coords: Genes x strands x used bins:", gene_df.shape[0])
    if gene_df.isnull().values.any() == True:
        print("Warning: Merging with with gene coords didn't work well; there are NaNs")
    
    ###count_Nucl_in_a_fragment_relative_coords(Nucl_of_interest, gen_ref, chrom, TSS, TES, gene_strand, ref_strand, bin_start, binsize)
    gene_df.loc[:, "Nucl_count"] = gene_df.apply(lambda x: count_Nucl_in_a_fragment_relative_coords(Nucl_of_interest, gen_ref, x["Chr"], x["TSS"], x["TES"], x["Strand"], x["ref_strand"], x["Bin"], BINSIZE), axis = 1)
    
    if gene_df["Nucl_count"].isnull().values.any() == True:
        print("Warning: Nucl counting didn't work well; there are NaNs")
    print("Number of zero-Nucl bins:", gene_df[gene_df["Nucl_count"] == 0].shape[0])
    gene_df.loc[:, "Nucl_count"] = gene_df["Nucl_count"].replace(0, np.nan)

    gene_df = gene_df.loc[:, ["Gene", "Strand", "Bin", "Nucl_count"]]
    
    #summing up the values per bin
    DATA1 = DATA1.loc[:, ["Sample", "Gene", "Strand", "Bin", "Value"]]
    DATA1 = DATA1.groupby(by = ["Sample", "Gene", "Strand", "Bin"]).sum().reset_index()
        
    tmp = pd.merge(DATA1, template, on = ("Sample", "Strand", "Gene", "Bin"), how = "right").fillna(0)
    print(tmp.shape[0])
    print(tmp.shape[0]/(len(strands)*len(bin_list)*len(sample_list)))
    print("Mapping data; Number of zero-Nucl bins:", tmp[tmp["Value"] == 0].shape[0])
    
    #correcting values by the number of G per bin
    tmp = pd.merge(tmp, gene_df, on = ["Gene", "Strand", "Bin"], how = "inner")
    print("After merging with Nucl counts:", tmp.shape[0])
    
    tmp.loc[:, 'Value'] = (10**3)*tmp['Value']/tmp['Nucl_count']
    
    print("Mapping data after Nucl corr.; Number of zero-Nucl bins:", tmp[tmp["Value"] == 0].shape[0])
    print("Mapping data after Nucl corr.; Number of NaN bins (devisions by zero):", tmp[tmp["Value"].isna()].shape[0])
    

    if tmp[tmp["Value"] == np.inf].shape[0] > 0:
        print("Warning: Infs!")
    if tmp[tmp["Value"] == -1*np.inf].shape[0] > 0:
        print("Warning: Infs!")
        
    tmp.loc[:, "Bin"] = tmp["Bin"] + BINSIZE/2.0
    tmp = tmp.sort_values(by = ["Sample", "Strand", "Gene", "Bin"])
    tmp = tmp.reset_index(drop = True)
        
    tmp.to_csv(OUTPATH + "Bin_" + str(BINSIZE) + SUFFIX + ".csv")
    

###
#Part 2. Binning with absolute coordinates.
###
def count_Nucl_in_a_fragment_abs_coords(Nucl_of_interest, gen_ref, chrom, gene_start, gene_end, gene_strand, ref_strand, bin_start, binsize, REF_POINT):
    compl_hash = {"A" : "T", "T" : "A", "C" : "G", "G" : "C"}
    #nucleotide to count in the sequence to be extracted 
    nucl = None
    if ref_strand == "+":
        if gene_strand == "sense":
            nucl = Nucl_of_interest#"G"
        if gene_strand == "antisense":
            nucl = compl_hash[Nucl_of_interest]#"C"
    if ref_strand == "-":
        if gene_strand == "sense":
            nucl = compl_hash[Nucl_of_interest]#"C"
        if gene_strand == "antisense":
            nucl = Nucl_of_interest#"G"
    
    origin = None
    if (REF_POINT == "TSS") and (ref_strand == "+"):
        origin = gene_start
    if (REF_POINT == "TSS") and (ref_strand == "-"):
        origin = (gene_end-1)
    if (REF_POINT == "TES") and (ref_strand == "+"):
        origin = (gene_end-1)
    if (REF_POINT == "TES") and (ref_strand == "-"):
        origin = gene_start
    
    START = None
    END = None
    
    if ref_strand == "+":
        START = bin_start + origin
        END = (bin_start+binsize) + origin
        #print(START, END)
        #the lowest boundary is not inclusive, to match the pd.cut settings
        START = START + 1
        #print(START, END)
    if ref_strand == "-":
        START = -1*bin_start + origin
        END = -1*(bin_start+binsize) + origin
        #print(START, END)
        #the lowest boundary is not inclusive, to match the pd.cut settings
        START = START - 1
        #print(START, END)
        
    sequence = None
    #+1: due to the property of the module pyfaidx
    if ref_strand == "+":
        sequence = gen_ref[chrom][START:(END+1)].seq
    if ref_strand == "-":
        sequence = gen_ref[chrom][END:(START+1)].seq
        
    sequence = sequence.upper()
    Nnucl = sequence.count(nucl)
    
    return Nnucl
    #print(sequence, Nnucl)

    
def binning_beyond(Nucl_of_interest, DATA1, BINSIZE, BORDERS, VARIABLE, norm_df, SUFFIX, gen_ref, REF_POINT, DF_gene_coords, OUTPATH):
    #creating bins
    bin_borders = np.arange(BORDERS[0], BORDERS[1] + 1, BINSIZE)
    print(bin_borders)
    
    #building a template for the final data frame
    strands = ["sense", "antisense"]
    bin_list = list(bin_borders[:-1])
    sample_list = DATA1["Sample"].unique().tolist()
    genes = DF_gene_coords["Gene"].unique().tolist()
    
    template = pd.DataFrame(
                        list(product(sample_list, strands, genes, bin_list)),
                        columns=['Sample', 'Strand', 'Gene', 'Bin'])
    print("Template for the final data frame:", template.shape[0])
    
    #building a template data frame for Nucl counting
    template_for_Nucl = pd.DataFrame(
                        list(product(strands, genes, bin_list)),
                        columns=['Strand', 'Gene', 'Bin'])
    print("Template data frame for Nucl counting; Genes x strands x used bins:", template_for_Nucl.shape[0])
    
    #binning
    DATA1.loc[:, 'Bin'] = pd.cut(DATA1[VARIABLE], bins = bin_borders, labels = bin_borders[:-1], 
                                 include_lowest = False, right = True)#include_lowest is not False!!!
    DATA1.loc[:, 'Bin'] = DATA1['Bin'].astype(int)### Pay attention that the bin borders are integers
    
    #correcting the values for the sequencing depth
    DATA1 = pd.merge(DATA1, norm_df, on = "Sample", how = "left")
    DATA1.loc[:, 'Value'] = DATA1['Value']/DATA1['Median']
    
    #counting the number of Nucl per bin
    gene_df = pd.merge(DF_gene_coords, template_for_Nucl, on = ['Gene'], how = "inner")
    print("Template data frame for Nucl counting after merging with gene coords; Genes x strands x used bins:", gene_df.shape[0])
    if gene_df.isnull().values.any() == True:
        print("Warning: Merging with gene coords didn't work well; there are NaNs")
        
    gene_df.loc[:, "Nucl_count"] = gene_df.apply(lambda x: count_Nucl_in_a_fragment_abs_coords(Nucl_of_interest, gen_ref, x["Chr"], x["Gene_start"], x["Gene_end"], x["Strand"], x["ref_strand"], x["Bin"], BINSIZE, REF_POINT), axis = 1)
    
    if gene_df["Nucl_count"].isnull().values.any() == True:
        print("Warning: Nucl counting didn't work well; there are NaNs")
    print("Number of Nucl-zero bins:", gene_df[gene_df["Nucl_count"] == 0].shape[0])
    gene_df.loc[:, "Nucl_count"] = gene_df["Nucl_count"].replace(0, np.nan)
    
    gene_df = gene_df.loc[:, ["Gene", "Strand", "Bin", "Nucl_count"]]
    
    #summing up the values per bin
    DATA1 = DATA1.loc[:, ["Sample", "Gene", "Strand", "Bin", "Value"]]
    DATA1 = DATA1.groupby(by = ["Sample", "Gene", "Strand", "Bin"]).sum().reset_index()
    
    tmp = pd.merge(DATA1, template, on = ("Sample", "Strand", "Gene", "Bin"), how = "right").fillna(0)
    print(tmp.shape[0])
    print(tmp.shape[0]/(len(strands)*len(bin_list)*len(sample_list)))
    print("Mapping data; Number of Nucl-zero bins:", tmp[tmp["Value"] == 0].shape[0])
    
    #correcting values by the number of Nucl per bin
    tmp = pd.merge(tmp, gene_df, on = ["Gene", "Strand", "Bin"], how = "inner")
    print("After merging with Nucl counts:", tmp.shape[0])
    
    tmp.loc[:, 'Value'] = (10**3)*tmp['Value']/tmp['Nucl_count']
    
    print("Mapping data after Nucl corr.; Number of zero-Nucl bins:", tmp[tmp["Value"] == 0].shape[0])
    print("Mapping data after Nucl corr.; Number of NaN bins (devision by zero):", tmp[tmp["Value"].isna()].shape[0])
    
    if tmp[tmp["Value"] == np.inf].shape[0] > 0:
        print("Warning: Infs!")
    if tmp[tmp["Value"] == -1*np.inf].shape[0] > 0:
        print("Warning: Infs!")
    
    
    tmp.loc[:, "Bin"] = tmp["Bin"] + BINSIZE/2.0
            
    tmp = tmp.sort_values(by = ["Sample", "Strand", "Gene", "Bin"])
    tmp = tmp.reset_index(drop = True)
    
        
    tmp.to_csv(OUTPATH + "Bin_" + str(BINSIZE) + SUFFIX + ".csv")


def count_Nucl_in_a_fragment_abs_coords_mDNA(Nucl_of_interest, gen_ref, chrom, start, end, strand):
    compl_hash = {"A" : "T", "T" : "A", "C" : "G", "G" : "C"}
    #nucleotide to count in the sequence to be extracted 
    nucl = None
    if strand == "+":
        nucl = Nucl_of_interest#"G"
    if strand == "-":
        nucl = compl_hash[Nucl_of_interest]#"C"
    
    START = start
    if START > 0:
        START = START + 1 #the lowest boundary is not inclusive except for 0: to match the pd.cut settings
    END = end
    
    #+1: due to the property of the module pyfaidx
    sequence = gen_ref[chrom][START:(END+1)].seq
    sequence = sequence.upper()
    
    Nnucl = sequence.count(nucl)

    #print(sequence, Nnucl)
    return Nnucl

def binning_Nucl_in_mDNA(Nucl_of_interest, DATA1, BINSIZE, BORDERS, VARIABLE, SUFFIX, gen_ref, OUTPATH):
    #creating bins
    bin_borders = list(np.arange(BORDERS[0], BORDERS[1], BINSIZE))#BORDERS[1] is chromosome length here
    
    if bin_borders[-1] != BORDERS[1] - 1:
        bin_borders += [BORDERS[1] - 1]
    bin_borders = np.array(bin_borders)
    print(bin_borders)
                    
    bin_lengths = np.diff(bin_borders)
    template_with_bin_sizes = pd.DataFrame({"Bin" : bin_borders[:-1], "Bin_size" : np.diff(bin_borders)})
    
    #building a template for the final data frame
    strands = ["+", "-"]
    bin_list = list(bin_borders[:-1])
    sample_list = DATA1["Sample"].unique().tolist()
    
    template = pd.DataFrame(
                        list(product(sample_list, strands, bin_list)),
                        columns=['Sample', 'Strand', 'Bin'])
    print("Template for the final data frame:", template.shape[0])
    
    #building a template data frame for Nucl counting
    template_for_Nucl = pd.DataFrame(
                        list(product(strands, bin_list)),
                        columns=['Strand', 'Bin'])
    print("Template data frame for Nucl counting; Strands x used bins:", template_for_Nucl.shape[0])
    template_for_Nucl = pd.merge(template_for_Nucl, template_with_bin_sizes, on = "Bin")
    print("Template data frame for Nucl counting; Strands x used bins with bin sizes:", template_for_Nucl.shape[0])
    
    #binning
    DATA1.loc[:, 'Bin'] = pd.cut(DATA1[VARIABLE], bins = bin_borders, labels = bin_borders[:-1], 
                                 include_lowest = True, right = True)#include_lowest is True!!!
    DATA1['Bin'] = DATA1['Bin'].astype(int)### Pay attention that the bin borders are integers
    
    #counting the number of Nucl per bin
    #count_Nucl_in_a_fragment_abs_coords_mDNA(Nucl_of_interest, gen_ref, chrom, start, end, strand)
    template_for_Nucl.loc[:, "Nucl_count"] = template_for_Nucl.apply(lambda x: count_Nucl_in_a_fragment_abs_coords_mDNA(Nucl_of_interest, gen_ref, "chrM", x["Bin"], x["Bin"] + x["Bin_size"], x["Strand"]), axis = 1)
    
    if template_for_Nucl["Nucl_count"].isnull().values.any() == True:
        print("Warning: Nucl counting didn't work well; there are NaNs")
    print("Number of Nucl-zero bins:", template_for_Nucl[template_for_Nucl["Nucl_count"] == 0].shape[0])
    template_for_Nucl.loc[:, "Nucl_count"] = template_for_Nucl["Nucl_count"].replace(0, np.nan)
    
    template_for_Nucl = template_for_Nucl.loc[:, ["Strand", "Bin", "Nucl_count", "Bin_size"]]
    
    #summing up the values per bin
    DATA1 = DATA1.loc[:, ["Sample", "Strand", "Bin", "Value"]]
    DATA1 = DATA1.groupby(by = ["Sample", "Strand", "Bin"]).sum().reset_index()
    
    tmp = pd.merge(DATA1, template, on = ("Sample", "Strand", "Bin"), how = "right").fillna(0)
    print(tmp.shape[0])
    print(tmp.shape[0]/(len(strands)*len(bin_list)*len(sample_list)))
    print("Mapping data; Number of Nucl-zero bins:", tmp[tmp["Value"] == 0].shape[0])
    
    #correcting values by the number of G per bin
    tmp = pd.merge(tmp, template_for_Nucl, on = ["Strand", "Bin"], how = "inner")
    print("After merging with Nucl counts:", tmp.shape[0])
    
    tmp.loc[:, 'Value'] = (10**3)*tmp['Value']/tmp['Nucl_count']
    
    print("Mapping data after Nucl corr.; Number of zero-Nucl bins:", tmp[tmp["Value"] == 0].shape[0])
    print("Mapping data after Nucl corr.; Number of NaN bins (devision by zero):", tmp[tmp["Value"].isna()].shape[0])
    
    if tmp[tmp["Value"] == np.inf].shape[0] > 0:
        print("Warning: Infs!")
    if tmp[tmp["Value"] == -1*np.inf].shape[0] > 0:
        print("Warning: Infs!")
    
    tmp["Bin"] = tmp["Bin"].astype(np.float64)### this is necessary for pandas 2.2.2
    tmp.loc[:, "Bin"] = tmp["Bin"] + tmp["Bin_size"]/2.0
            
    tmp = tmp.sort_values(by = ["Sample", "Strand", "Bin"])
    tmp = tmp.reset_index(drop = True)
    
        
    tmp.to_csv(OUTPATH + "Bin_" + str(BINSIZE) + "_mDNA_CCS.v3_MS_" + SUFFIX + ".csv")

    