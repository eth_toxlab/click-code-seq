import numpy as np
import pandas as pd
from itertools import product

def remove_centromeric_and_gap_bins(tempDF, BINSIZE, cenSatRegions, gaps, chromosomes):
    #cenSatRegions = "/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/genome_annotation/general_human_annotation/Centromeres_GRCh38.hg38.UCSCTableBrowser_downl31.01.2023.csv"
    #gaps = "/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/genome_annotation/general_human_annotation/Gaps_GRCh38.hg38.downl01.02.2023.csv"

    df_centromere = pd.read_csv(cenSatRegions, header = 0, sep = '\t')
    df_centromere = df_centromere[df_centromere["chrom"].isin(chromosomes)]

    for row_i, row in df_centromere.iterrows():
        chrom = row["chrom"]
        st = float(row["chromStart"])
        end = float(row["chromEnd"])

        tempDF = tempDF[~((tempDF["Chromosome"] == chrom) & (tempDF["Bin"] >= st) & (tempDF["Bin"] <= end))].copy()
        tempDF = tempDF[~((tempDF["Chromosome"] == chrom) & (tempDF["Bin"] + BINSIZE >= st) & (tempDF["Bin"] + BINSIZE <= end))].copy()

    df_gaps = pd.read_csv(gaps, header = 0, sep = "\t")
    df_gaps = df_gaps[df_gaps["chrom"].isin(chromosomes)]
    df_gaps = df_gaps[df_gaps["type"].isin(['heterochromatin', 'short_arm'])]

    for row_i, row in df_gaps.iterrows():
        chrom = row["chrom"]
        st = float(row["chromStart"])
        end = float(row["chromEnd"])

        tempDF = tempDF[~((tempDF["Chromosome"] == chrom) & (tempDF["Bin"] >= st) & (tempDF["Bin"] <= end))].copy()
        tempDF = tempDF[~((tempDF["Chromosome"] == chrom) & (tempDF["Bin"] + BINSIZE >= st) & (tempDF["Bin"] + BINSIZE <= end))].copy()

    return(tempDF)


def bin_chromatin_mark_peaks(BINSIZEs, df, chromosomes, DF_chrsizes, OUTPATH, PREFIX):
    epi_marks = df["Study"].unique()

    df.loc[:, 1] = df[1].astype(float)
    df.loc[:, 2] = df[2].astype(float)

    df = df[df[0].isin(chromosomes)]
    df.loc[:, "Length"] = df[2] - df[1]
    if df[df["Length"] <= 0].shape[0] > 0:
        print("Warning: non-positive length!")
    #
    print("Mean:")
    print(df.loc[:, ["Study", "Length"]].groupby(by = "Study").mean())
    print("Max:")
    print(df.loc[:, ["Study", "Length"]].groupby(by = "Study").max())
    print("Min:")
    print(df.loc[:, ["Study", "Length"]].groupby(by = "Study").min())
    #

    for BINSIZE in BINSIZEs:
        print(BINSIZE)
        DF_epi_binned = pd.DataFrame({})
        for chromosome in chromosomes:
            print(chromosome)
            chr_length = float(DF_chrsizes[DF_chrsizes[0] == chromosome][1])
            bin_borders = list(np.arange(0, chr_length, BINSIZE))

            if bin_borders[-1] != chr_length - 1:
                bin_borders += [chr_length - 1]
            bin_borders = np.array(bin_borders)

            sizes_array = np.diff(bin_borders)
            sizes_array = [sizes_array[0] + 1] + list(sizes_array[1:])#we made the left boundary of the first bin inclusive, therefore, the length is 1 bp bigger
            template_binsizes = pd.DataFrame({"Bin" : bin_borders[:-1], "Bin_size" : sizes_array})
            template = pd.DataFrame(
                                list(product(epi_marks, bin_borders[:-1])),
                                columns=['Study', 'Bin'])
            template = pd.merge(template, template_binsizes, on = "Bin", how = "left")
            #print(template)

            df_ch = df[df[0] == chromosome].copy().reset_index(drop = True)
            #print("Input number of peaks:", df_ch.shape[0])

            if df_ch.shape[0] > 0:
                df_ch.loc[:, 'Start_bin'] = pd.cut(df_ch[1], bins = bin_borders, labels = bin_borders[:-1], 
                                                               include_lowest = True, right = True)
                df_ch.loc[:, 2] = df_ch[2] - 1###in the original bed file, the right border is not inclusive
                df_ch.loc[:, 'End_bin'] = pd.cut(df_ch[2], bins = bin_borders, labels = bin_borders[:-1], 
                                                             include_lowest = True, right = True)
                #print(df_ch.shape[0])
                
                if df_ch.shape[0] - df_ch.dropna().shape[0] > 0:
                    print("Warning: there are NaNs after pd.cut of the peaks ends!")

                df_ch.loc[:, 'Start_bin'] = df_ch['Start_bin'].astype(float)
                df_ch.loc[:, 'End_bin'] = df_ch['End_bin'].astype(float)

                #This numner + 1 is the number of bins that a peak overlaps with
                df_ch.loc[:, 'Bin_number'] = (df_ch['End_bin'] - df_ch['Start_bin'])/BINSIZE
                bin_numbers = np.sort(df_ch['Bin_number'].unique())
                print("Numbers of bins covered by peaks - 1:", bin_numbers)

                tmp1 = pd.DataFrame({})
                for BN in bin_numbers:
                    for i_BN in np.arange(0, BN+1, 1):
                        tmp2 = df_ch[df_ch['Bin_number'] == BN].copy()
                        tmp2.loc[:, "Bin"] = tmp2['Start_bin'] + i_BN*BINSIZE
                        tmp2.loc[:, "Bin_lo"] = tmp2['Bin'] + 1*(tmp2['Bin'] > 0)#left boundary is not inclusive, except for the first bin
                        tmp2.loc[:, "Bin_up"] = tmp2['Start_bin'] + (i_BN + 1)*BINSIZE
                        tmp2.loc[:, "Value"] = tmp2[[2, 'Bin_up']].min(axis=1) - tmp2[[1, 'Bin_lo']].max(axis=1) + 1
                        tmp2 = tmp2.loc[:, ["Value", "Bin", "Study"]]
                        tmp1 = pd.concat([tmp1, tmp2])

                df_ch = None

                if tmp1[tmp1["Value"] < 0].shape[0] > 0:
                    print("Warning: negative values!")

                tmp1 = tmp1.reset_index(drop = True)
                tmp1 = tmp1.groupby(by = ["Bin", "Study"]).sum().reset_index()
                tmp1 = pd.merge(template, tmp1, on = ("Bin", "Study"), how = "left").fillna(0)

                tmp1.loc[:, "Chromosome"] = chromosome
                tmp1.loc[:, "Value"] = tmp1["Value"]/tmp1["Bin_size"]

                DF_epi_binned = pd.concat([DF_epi_binned, tmp1])

        DF_epi_binned = DF_epi_binned.reset_index(drop = True)
        #print(DF_epi_binned)
        DF_epi_binned.to_csv(OUTPATH + PREFIX + str(int(BINSIZE)) + ".csv")