#Vakil Takhaveev, PhD
#06.02.2024
#
#This is a collection of figure-plotting functions.

#importing necessary modules
import os
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import sys

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

matplotlib.rcParams['font.sans-serif'] = "Arial"
matplotlib.rcParams['font.family'] = "sans-serif"
matplotlib.rcParams['mathtext.default'] = "regular"

matplotlib.rcParams['font.size'] = 16



def bounds_of_groups(DATA1, perc_list):
    #perc_list - sorted increasing list of upper-border percentiles
    
    tmp = DATA1.loc[DATA1["Expression_level"] != 0, ["Gene", "Expression_level"]].copy().drop_duplicates()
    expr_array = np.array(tmp["Expression_level"])
    #print(len(expr_array))
    
    if np.max(expr_array) != np.percentile(expr_array, 100):
        print("Warning!")
    
    bounds = [(-1, 0)]
    prev_value = 0
    
    for p in perc_list:
        p_value = np.percentile(expr_array, p)
        
        bounds.append((prev_value, p_value))
        prev_value = p_value
    
    bins = pd.IntervalIndex.from_tuples(bounds)
    return(bins)


def plot_damage_boxplots(DATA1, samples, groups, feature_order, palette_features, SUFFIX, ylims, yticks, ylabel, FIGURE_OUTPATH):
    for index, s in enumerate(samples):
        df = DATA1[DATA1["Sample"] == s].copy()
        
        print(s)
        tmp = df.loc[df["Feature"] == feature_order[0], ["Group"]].groupby(by = "Group").size()
        print(tmp, tmp.sum())
        print("Not shown:", feature_order[0], df[(df["Feature"] == feature_order[0]) & ((df["Damage"] > ylims[1]) | (df["Damage"] < ylims[0]))].shape[0])
        print("Not shown:", feature_order[1], df[(df["Feature"] == feature_order[1]) & ((df["Damage"] > ylims[1]) | (df["Damage"] < ylims[0]))].shape[0])
        
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        
        ax = plt.subplot(1, 1, 1)
        sns.boxplot(x = "Group", y = "Damage", hue = "Feature", data = df, 
                    order = groups,
                    hue_order = feature_order,
                    orient = "v", fliersize = 0.2, linewidth = 1, saturation = 1,
                    palette = palette_features, showmeans=True,
                    meanprops={"marker" : "*", "markerfacecolor" : "None", "markeredgecolor" : "#2F4F4F", "markersize" : 5},
                    flierprops={"markerfacecolor" : "gray", "markeredgecolor" : "gray"})

        
        t = ax.text(1, 0.02, "1 replicate", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.75, edgecolor='None', linewidth=0))

        leg = ax.legend(loc = 2, frameon = True, fontsize = "medium")
        leg.get_frame().set_linewidth(0.0)

        ax.set_ylabel('')
        ax.set_xlabel("Gene expression tiers")
        ax.set_xticklabels(groups, rotation = 45)
        ax.set_ylim(ylims[0], ylims[1])
        ax.set_yticks(yticks)
        
        #this is to isolate the first group of all genes:
        ax.axvline(0.5, ls = 'dashed', color = sns.color_palette("bright")[7], lw = 1)
        
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        
        fig.supylabel(t = ylabel, ha = "center", va = "center",
                      x = 0.05, y = 0.65, fontsize = "medium")
        
        plt.tight_layout(pad = 0.25)
        
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + s.replace(" ", "_") + "_boxplots_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + s.replace(" ", "_") + "_boxplots_" + SUFFIX + ".png")
        plt.close(fig)  
    
    
def plot_median_damage_values_and_correlation(DATA1, sample_group, groups, feature_order, palette_features, SUFFIX, 
                                            ylims, yticks, ylabel_part, FLAG_correlation, feature_order_abbrs, yticks_corr, FIGURE_OUTPATH):
    feat_list = []
    i_list = []
    gr_list = []
    median_list = []
    mean_list = []

    rho_list = []
    sample_list = []
    feat_for_rho_list = []

    for index, feat in enumerate(feature_order):
        for s in sample_group:
            df = DATA1[DATA1["Sample"] == s].copy()
            df1 = df[df["Feature"] == feat].copy()
                
            rho, p_val = scipy.stats.spearmanr(df1["Expression_level"], df1["Damage"])
            print("Spearman rho:", feat, s, rho, p_val)
            rho_list.append(rho)
            sample_list.append(s)
            feat_for_rho_list.append(feat)
            
            list_of_lists = []
                
            for i, gr in enumerate(groups):
                l1 = df1[df1["Group"] == gr]["Damage"].values
                list_of_lists.append(list(l1))
                
                M = np.median(l1)
                mean = np.mean(l1)
                    
                median_list.append(M)
                mean_list.append(mean)
                i_list.append(i)
                gr_list.append(gr)
                feat_list.append(feat)
                
            print("Kruskal:", feat, s, scipy.stats.kruskal(*list_of_lists))
            
    tmp = pd.DataFrame({"Group" : gr_list, "i" : i_list, "Median" : median_list, "Feature" : feat_list, "Mean" : mean_list})
    tmp = tmp.sort_values(by = ["Group"], ascending = True)
    tmp = tmp.sort_values(by = ["Feature"], ascending = True)
    print(tmp)
    
    tmp_rho = pd.DataFrame({"Sample" : sample_list, "Feature" : feat_for_rho_list, "Rho" : rho_list})
    tmp_rho.loc[:, "Type"] = SUFFIX
    
    metrics_list = ["Median", "Mean"]
    markers_list = ["o", "*"]
    
    for metric_index, metric in enumerate(metrics_list):
    
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        label = None
        prev_lab = None
        for j, row in tmp.iterrows():
            if row["i"] == 0:
                if row["Feature"] != prev_lab:
                    label = row["Feature"]
                else:
                    label = None

                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = label, lw = 0)

                prev_lab = row["Feature"]
            else:
                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = None, lw = 0)

        plt.legend(loc = 2, bbox_to_anchor = (-0.06, 0., 1, 1), frameon = False)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        lll = sorted(list(tmp["i"].unique()))
        print(lll)
        ax.set_xticks(lll)
        ax.set_xticklabels(groups, rotation = 45)
        ax.set_xlabel("Gene expression tiers")
        ax.set_ylabel("")

        ax.set_ylim(ylims[0], ylims[1])
        ax.set_yticks(yticks)
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)

        fig.supylabel(t = metric + ' ' + ylabel_part + ' level\nin genes (arb. unit)', ha = "center", va = "center", x = 0.05, y = 0.6, fontsize = "medium")

        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + metric + "_DNA_damage_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + metric + "_DNA_damage_" + SUFFIX + ".png")
        plt.close(fig)
        
    
    if FLAG_correlation == True:
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        for s in (tmp_rho["Sample"].unique()):
            xx = []
            yy = []
            for i_f, f in enumerate(feature_order):
                y = tmp_rho[(tmp_rho["Sample"] == s) & (tmp_rho["Feature"] == f)]["Rho"].iat[0]
                xx += [i_f]
                yy += [y]

                ax.plot([i_f], [y], 'o', color = palette_features[f], alpha = 1, markersize = 12)

            ax.plot(xx, yy, '-', color = "gray", alpha = 1, lw = 0.5)

        ax.set_xlabel("Strand")
        ax.set_ylabel("")
        ax.set_xticks([0, 1])
        ax.set_xlim(-0.5, 1.5)
        ax.set_xticklabels(feature_order_abbrs)
        ax.set_yticks(yticks_corr)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        fig.supylabel(t = 'Correlation between ' + ylabel_part + ' level\nand gene expression', ha = "center", va = "top",
                          x = 0.05, y = 0.99, fontsize = "medium")

        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".png")
        plt.close(fig)
           
            
def plot_metaprofile_gene_and_beyond_SAMPLE_GROUPS(DATA1, sample_groups, xlims, ylims, ylabel, marks, mark_labels,
                                                   SD_df, binsizes, yticks, title, hue_palette, highlight_coords, FIGURE_OUTPATH):
    for sg in sample_groups:
        print(sg)
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)
        
        samples = sample_groups[sg]
        tmp = SD_df[SD_df["Sample"].isin(samples)].sort_values(by = "Sample").copy()
        tmp.loc[:, "Rank"] = scipy.stats.rankdata(1.0/tmp["Median"], method = "ordinal") - 1 
        print(tmp)
        linestyles = ["solid", "dashed", "dotted"]
        
        for index, s in enumerate(samples):
            df = DATA1[DATA1["Sample"] == s].copy()
            
            linestyle = linestyles[tmp[tmp["Sample"] == s]["Rank"].iat[0]]
            
            legend  = False
            if linestyle == "solid":
                legend = "full"

            sns.lineplot(x = "Bin", y = "Value", hue = "Feature", data = df,
                            hue_order = ["Non-transcribed", "Transcribed"], estimator=np.mean, ci = 95,
                            palette = hue_palette, linestyle = linestyle, legend = legend)

        leg = ax.legend(loc = 4, frameon = False, title = "Strands:", 
                            labelspacing = 0.1, bbox_to_anchor=(0.5, 0.05, 0.5, 0.9), fontsize = "medium")
        leg._legend_box.align='left'
        ax.text(0.5, 1, title, ha = "center", va = "top", transform=ax.transAxes, fontsize = "medium")
        
        N = len(samples)
        ax.text(1, 0.95, str(N) + " replicates", ha = "right", va = "top", transform=ax.transAxes, fontsize = "medium")
        ax.set_ylabel("")
        ax.set_xlabel("")
        ax.set_ylim(ylims[sg][0], ylims[sg][1])
        ax.set_xlim(xlims[0], xlims[1])
        ax.axvline(marks[1], ymax = 0.02, ls = "-", color = "black", lw = 1, zorder = -2)
        ax.axvline(marks[3], ymax = 0.02, ls = "-", color = "black", lw = 1, zorder = -2)

        ax.set_xticks(marks)
        ax.set_xticklabels(mark_labels)
        ax.set_yticks(yticks[sg])
        
        ax.text(0.5*(marks[0]+marks[1]), 0.005*(ylims[sg][1] - ylims[sg][0]) + ylims[sg][0], binsizes[0], 
                    ha = "center", va = "bottom", color = "gray", fontsize = "medium")
        ax.text(0.5*(marks[1]+marks[3]), 0.005*(ylims[sg][1] - ylims[sg][0]) + ylims[sg][0], binsizes[1], 
                    ha = "center", va = "bottom", color = "gray", fontsize = "medium")
        ax.text(0.5*(marks[3]+marks[4]), 0.005*(ylims[sg][1] - ylims[sg][0]) + ylims[sg][0], binsizes[2], 
                    ha = "center", va = "bottom", color = "gray", fontsize = "medium")

        ax.text(marks[0] + 0.4*(marks[1] - marks[0]), 0.06*(ylims[sg][1] - ylims[sg][0]) + ylims[sg][0], "Bins:", 
                    ha = "center", va = "bottom", color = "gray", fontsize = "medium")
        
        #
        ax.axvline(highlight_coords[0], ls = "-", ymin = highlight_coords[2], ymax = highlight_coords[3], color = "#A9A9A9", lw = 1, zorder = -2)
        ax.axvline(highlight_coords[1], ls = "-", ymin = highlight_coords[2], ymax = highlight_coords[3], color = "#A9A9A9", lw = 1, zorder = -2)
        #
        
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        
        fig.supylabel(t = ylabel, ha = "center", va = "top",
                          x = 0.03, y = 0.98, fontsize = "medium")
        
        plt.tight_layout(pad = 0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + sg.replace(" ", "_") + "_metaprofile_" + title.replace(" ", "_").replace("%", "pc") + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + sg.replace(" ", "_") + "_metaprofile_" + title.replace(" ", "_").replace("%", "pc") + ".png")
        plt.close(fig)
                    

            
def plot_metaprofile_aroundTSS_SAMPLE_GROUPS(DATA1, sample_groups, SD_df, xlims, ylims, ylabel, yticks,
                                            marks, mark_labels, binsizes, title, hue_palette, highlight_coords, 
                                            FIGURE_OUTPATH):
    for sg in sample_groups:
        print(sg)
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)
        
        samples = sample_groups[sg]
        
        tmp = SD_df[SD_df["Sample"].isin(samples)].sort_values(by = "Sample").copy()
        if tmp.shape[0] > 3:
            print("Warning")
        tmp.loc[:, "Rank"] = scipy.stats.rankdata(1.0/tmp["Median"], method = "ordinal") - 1 
        print(tmp)
        linestyles = ["solid", "dashed", "dotted"]
        
        for index, s in enumerate(samples):
            df = DATA1[DATA1["Sample"] == s].copy()
            
            linestyle = linestyles[tmp[tmp["Sample"] == s]["Rank"].iat[0]]
            
            legend  = False
            if linestyle == "solid":
                legend = "full"

            sns.lineplot(x = "Bin", y = "Value", hue = "Feature", data = df,
                            hue_order = ["Non-transcribed", "Transcribed"], estimator=np.mean, ci = 95,
                            palette = hue_palette, linestyle = linestyle, legend = legend)
        
        leg = ax.legend(loc = 4, frameon = False, title = "Strands:", 
                            labelspacing = 0.1, bbox_to_anchor=(0.7, 0.05, 0.3, 0.9), fontsize = "medium")
        leg.get_texts()[0].set_text('NT')
        leg.get_texts()[1].set_text('T')

        leg._legend_box.align='left'
        
        ax.text(0.5, 1, title, ha = "center", va = "top", transform=ax.transAxes, fontsize = "medium")
        
        N = len(samples)
        t = ax.text(0.025, 0.4, str(N) + " replicates", ha = "left", va = "top", transform=ax.transAxes, fontsize = "medium")
        #t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))
        
        ax.set_ylabel("")
        ax.set_xlabel("")
        ax.set_ylim(ylims[0], ylims[1])
        ax.set_xlim(xlims[0], xlims[1])
        for m in marks:
            if m == 0:
                ax.axvline(m, ymax = 0.95, ls = "dashed", color = "black", lw = 1, zorder = -2)
                
        ax.set_xticks(marks)
        ax.set_xticklabels(mark_labels)
        ax.set_yticks(yticks)
        
        ax.text(0.025, 0.01, binsizes[0], ha = "left", va = "bottom", transform=ax.transAxes, color = "gray", fontsize = "medium")
        
        #
        ax.axvline(highlight_coords[0], ls = "-", ymin = highlight_coords[2], ymax = highlight_coords[3], color = "#A9A9A9", lw = 1, zorder = -2)
        ax.axvline(highlight_coords[1], ls = "-", ymin = highlight_coords[2], ymax = highlight_coords[3], color = "#A9A9A9", lw = 1, zorder = -2)
        #
        
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        
        #####
        fig.supylabel(t = ylabel, ha = "center", va = "center",
                          x = 0.03, y = 0.55, fontsize = "medium")
        
        plt.tight_layout(pad = 0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + sg.replace(" ", "_") + "_metaprofile_aroundTSS_" + binsizes[0].replace(" ", "_").replace("-", "_") + "_" + title.replace(" ", "_").replace("%", "pc") + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + sg.replace(" ", "_") + "_metaprofile_aroundTSS_" + binsizes[0].replace(" ", "_").replace("-", "_") + "_" + title.replace(" ", "_").replace("%", "pc") + ".png")
        plt.close(fig)
        
        
        
def plot_median_damage_values_and_correlation_Version2(DATA1, sample_group, groups, feature_order, palette_features, SUFFIX, 
                                            ylims, yticks, ylabel_part, FLAG_correlation, feature_order_abbrs, yticks_corr, FIGURE_OUTPATH):
    feat_list = []
    i_list = []
    gr_list = []
    median_list = []
    mean_list = []

    rho_list = []
    sample_list = []
    feat_for_rho_list = []

    for index, feat in enumerate(feature_order):
        for s in sample_group:
            df = DATA1[DATA1["Sample"] == s].copy()
            df1 = df[df["Feature"] == feat].copy()
                
            rho, p_val = scipy.stats.spearmanr(df1["Expression_level"], df1["Damage"])
            print("Spearman rho:", feat, s, rho, p_val)
            rho_list.append(rho)
            sample_list.append(s)
            feat_for_rho_list.append(feat)
            
            list_of_lists = []
                
            for i, gr in enumerate(groups):
                l1 = df1[df1["Group"] == gr]["Damage"].values
                list_of_lists.append(list(l1))
                
                M = np.median(l1)
                mean = np.mean(l1)
                    
                median_list.append(M)
                mean_list.append(mean)
                i_list.append(i)
                gr_list.append(gr)
                feat_list.append(feat)
                
            print("Kruskal:", feat, s, scipy.stats.kruskal(*list_of_lists))
            
    tmp = pd.DataFrame({"Group" : gr_list, "i" : i_list, "Median" : median_list, "Feature" : feat_list, "Mean" : mean_list})
    tmp = tmp.sort_values(by = ["Group"], ascending = True)
    tmp = tmp.sort_values(by = ["Feature"], ascending = True)
    print(tmp)
    
    tmp_rho = pd.DataFrame({"Sample" : sample_list, "Feature" : feat_for_rho_list, "Rho" : rho_list})
    tmp_rho.loc[:, "Type"] = SUFFIX
    
    metrics_list = ["Median", "Mean"]
    markers_list = ["o", "*"]
    
    for metric_index, metric in enumerate(metrics_list):
    
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        label = None
        prev_lab = None
        for j, row in tmp.iterrows():
            if row["i"] == 0:
                if row["Feature"] != prev_lab:
                    label = row["Feature"]
                else:
                    label = None

                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = label, lw = 0)

                prev_lab = row["Feature"]
            else:
                ax.plot(row["i"], row[metric], marker = markers_list[metric_index], markeredgecolor = palette_features[row["Feature"]], 
                        markerfacecolor = "None", alpha = 0.8,
                        markersize = 10, markeredgewidth = 2.5, label = None, lw = 0)

        plt.legend(loc = 2, bbox_to_anchor = (-0.06, 0., 1, 1), frameon = False)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        lll = sorted(list(tmp["i"].unique()))
        print(lll)
        ax.set_xticks(lll)
        ax.set_xticklabels(groups, rotation = 45)
        ax.set_xlabel("Gene expression tiers")
        ax.set_ylabel("")

        ax.set_ylim(ylims[0], ylims[1])
        ax.set_yticks(yticks)
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)

        ###This is the only difference in Version 2:
        fig.supylabel(t = metric + ' ' + ylabel_part, ha = "center", va = "center", x = 0.05, y = 0.6, fontsize = "medium")
        ###
        
        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + metric + "_DNA_damage_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + metric + "_DNA_damage_" + SUFFIX + ".png")
        plt.close(fig)
        
    
    if FLAG_correlation == True:
        fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
        ax = plt.subplot(1, 1, 1)

        for s in (tmp_rho["Sample"].unique()):
            xx = []
            yy = []
            for i_f, f in enumerate(feature_order):
                y = tmp_rho[(tmp_rho["Sample"] == s) & (tmp_rho["Feature"] == f)]["Rho"].iat[0]
                xx += [i_f]
                yy += [y]

                ax.plot([i_f], [y], 'o', color = palette_features[f], alpha = 1, markersize = 12)

            ax.plot(xx, yy, '-', color = "gray", alpha = 1, lw = 0.5)

        ax.set_xlabel("Strand")
        ax.set_ylabel("")
        ax.set_xticks([0, 1])
        ax.set_xlim(-0.5, 1.5)
        ax.set_xticklabels(feature_order_abbrs)
        ax.set_yticks(yticks_corr)
        t = ax.text(1, 0.02, str(len(sample_group)) + " replicates", ha = "right", va = "bottom", fontsize = "medium", transform=ax.transAxes)
        t.set_bbox(dict(facecolor='white', alpha=0.5, edgecolor='None', linewidth=0))

        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        fig.supylabel(t = 'Correlation between ' + ylabel_part + ' level\nand gene expression', ha = "center", va = "top",
                          x = 0.05, y = 0.99, fontsize = "medium")

        plt.tight_layout(pad=0.25)
        plt.savefig(FIGURE_OUTPATH + "PDFs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".pdf")
        plt.savefig(FIGURE_OUTPATH + "PNGs/" + "Corr_DNA_damage_vs_gene_expr_" + SUFFIX + ".png")
        plt.close(fig)
        
        
def plot_strand_bias_TSS_gene_body_TES(DATA1, sample_group, regions, palette, ylims, yticks, ylabel, SUFFIX, FIGURE_OUTPATH):
    df = DATA1[DATA1["Sample"].isin(sample_group)].copy()
    df = df[df["Region"].isin(regions)].copy()
    
    print("Not shown:", df[((df["Bias"] > ylims[1]) | (df["Bias"] < ylims[0]))].shape[0])
    
    fig = plt.figure(1, (1*5, 1*5), dpi = 200, facecolor = "white")
    ax = plt.subplot(1, 1, 1)
    N = len(sample_group)
    WIDTH = (N/3.0)*0.8
    
    sns.boxplot(x = "Region", y = "Bias", hue = "Sample", data = df, 
                    order = regions,
                    hue_order = sample_group,
                    orient = "v", width = WIDTH, palette = palette, saturation = 1, linewidth = 1, fliersize = 0.2, 
                    showmeans=True,
                    meanprops={"marker" : "*", "markerfacecolor" : "None", "markeredgecolor" : "#2F4F4F", "markersize" : 5},
                    flierprops={"markerfacecolor" : "gray", "markeredgecolor" : "gray"})
    
    legend = plt.legend(loc = 3, fontsize = "medium", title = "Replicates:", edgecolor = "None")
    for text in legend.get_texts():
        label = text.get_text()
        new_label = label.split("_")[-1]
        text.set_text(new_label)
    
    ax.set_ylabel('')
    ax.set_xlabel('')
    ax.set_ylim(ylims[0], ylims[1])
    ax.set_yticks(yticks)
    ax.axhline(0, ls = 'solid', color = "black", lw = 1)
    
    for i, region in enumerate(regions):
        for j, sample in enumerate(sample_group):
            data = df[(df['Region'] == region) & (df['Sample'] == sample)]['Bias']
            stat, p = scipy.stats.wilcoxon(data, alternative='two-sided', zero_method='wilcox', correction=True)
            print(region, sample, p)
            
            x = i - (WIDTH/2) + (WIDTH/(N*2)) + (WIDTH/N)*j
            col = "black"
            if p >= 0.05:
                col = "gray"
            ax.text(x, ylims[1] * 1, f'{p:.1e}', ha='left', va='top', fontsize='medium', rotation = 80,
                   color = col)
    
    ax.spines.right.set_visible(False)
    ax.spines.top.set_visible(False)
        
    fig.supylabel(t = ylabel, ha = "center", va = "center",
                      x = 0.05, y = 0.52, fontsize = "medium")
        
    plt.tight_layout(pad = 0.25)
        
    plt.savefig(FIGURE_OUTPATH + "PDFs/" + "Strand_bias_boxplots_" + SUFFIX + ".pdf")
    plt.savefig(FIGURE_OUTPATH + "PNGs/" + "Strand_bias_boxplots_" + SUFFIX + ".png")
    plt.close(fig)
